<?php
/**
 * @file
 * Hooks for Identity module.
 */

/**
 * Respond to a change in the current identity.
 * 
 *  @param $old_identity
 *   The previous identity, or NULL if there was one.
 *  @param $new_identity
 *   The new identity, or NULL if the identity is being cleared.
 */
function hook_identity_set($old_identity, $new_identity) {
  if ($new_identity != NULL) {
    drupal_set_message('Welcome @name', array('@name' => $new_identity->identifier));
  }
}
