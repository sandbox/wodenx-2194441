<?php
/**
 * @file
 * Identity module administration pages.
 */
function identity_settings($form, &$form_state) {
  $form['identity_timeout'] = array(
    '#type' => 'textfield',
    '#title' => t('Current identity persistence'),
    '#description' => t('The amount of time (in minutes) that the current identity should persist without activity. To mitigate the risk of identity theft on public computers, you should set this to a relatively short period if sensitive information is displayed to an identified user. Leave blank for no expiration. Set to zero to cause identity to persist only for a single request.'),
    '#default_value' => variable_get('identity_timeout', ''),
  );

  $form['identity_expiration'] = array(
    '#type' => 'textfield',
    '#title' => t('Saved identity persistence'),
    '#description' => t('The amount of time (in days) that a captured identity will be retained without activity. In general, to avoid database clutter, you will want to delete old identities who have not accessed the site after a certain period. Leave blank to preserve all identities.'),  
    '#default_value' => variable_get('identity_expiration', '120'),
  );

  $form['identity_unique_identifiers'] = array(
    '#type' => 'radios',
    '#title' => t('Duplicate identifiers'),
    '#options' => array(
      0 => t('Multiple identities may share the same identifier.'),
      1 => t('Each identity must have a unique identifier. A new identity replaces any existing identity with the same identifier.'),
      //'merge' => t('Not recommended. Each identity must have a unique identifier. A new identity is merged with any existing identity with the same identifer.'),
    ),
    '#description' => t('Specify what happens when a new identity is created with an identifer that is already in use.'), // Note that merging new identites with existing (the third option) may present a security risk if sensitive persional information associated with an identity is displayed to that identity.',
    '#default_value' => variable_get('identity_unique_identifiers', 0),
  );

  $form['identity_cookie'] = array(
    '#type' => 'textfield',
    '#title' => t('Cookie name'),
    '#description' => t('Specify the name of the cookie used to track a visitor\'s identity. Leave blank to use the normal Drupal session. Note that this will disable page caching for all identified users. If most of the content on your site is static for all users, regardless of identity, you may wish to use a unique cookie along with a mechanism (such as the @ce module) to exclude the dynamic pages.', array('!ce' => l('CacheExclude', 'https://drupal.org/project/cacheexclude'))),
    '#default_value' => variable_get('identity_cookie', ''),
  );

  return system_settings_form($form);
}
