<?php
/**
 * @file
 * Tokens for Identities
 */

/**
 * Implements hook_token_info().
 * !TODO Deprecate this in favor of entity api tokens.
 */ 
function identity_token_info() {
  $types['identity'] = array(
    'name' => t('Identity'),
    'description' => t('Tokens related to individual visitor identities.'),
    'needs-data' => 'identity',
  );
  $types['current-identity'] = array(
    'name' => t('Current identity'),
    'description' => t('Tokens related to the currently active identity.'),
    'type' => 'identity',
  );
  $tokens['identifier'] = array(
    'name' => t("Identifier"),
    'description' => t("The unique, human-readable ID -- usually an email address."),
  );
  $tokens['data'] = array(
    'name' => t('Data value'),
    'description' => t('The value of a specific data field of the current identity.'),
    'dynamic' => TRUE,
  );

  return array(
    'types' => $types,
    'tokens' => array('identity' => $tokens),
  );
}

/**
 * Implements hook_tokens().
 */ 
function identity_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  if ($type == 'identity' && !empty($data['identity'])) {
    $identity = $data['identity'];
    foreach ($tokens as $name => $original) {
      if ($name == 'identifier') {
        $replacements[$original] = !empty($options['sanitize']) ? check_plain($identity->identifier) : $identity->identifier;
      }
      elseif ($data_tokens = token_find_with_prefix($tokens, 'data')) {
        // !TODO Replace with entity_api tokens, but how do we make them dynamic?  Alternately, just use fields.
        foreach ($data_tokens as $name => $original) {
          $key = strtolower($name);
          if (isset($identity->data[$key])) {
            $replacements[$original] = $options['sanitize'] ? check_plain($identity->data[$key]) : $identity->data[$key];
          }
        }
      }
    }
  }

  if ($type == 'current-identity') {
    $identity = identity_get_current();
    $replacements += token_generate('identity', $tokens, array('identity' => $identity), $options);
  }

  return $replacements;
}
 
