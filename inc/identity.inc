<?php
/**
 * @file
 * Identity class definition.
 */

 /**
 * Primary class defining and Identity.
 */
class Identity extends Entity {
  public $iid; // Primary key - the numveric id of this identity.
  public $identifier; // Unique, human-readable identifier.
  public $uid; // The user who owns this identity, if any.
  public $created; // Timestamp of identity creation.
  public $accessed; // Timestamp of identity last access.
  public $data; // Serialized data array.

  /**
   * Constructor.
   */
  function __construct($values = NULL) {
    if (is_null($values)) {
      $values = uniqid();
    }
    if (is_string($values)) {
      $values = array(
        'identifier' => $values,
        'created' => REQUEST_TIME,
        'accessed' => REQUEST_TIME,
      );
    }
    parent::__construct($values, 'identity');
  }
}
