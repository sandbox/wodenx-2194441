<?php
/**
 * @file
* Plugin to provide access control based upon identity keys.
*/

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("Identity: has key"),
  'description' => t('Control access based on whether the current identity has a specified key.'),
  'callback' => 'identity_has_key_ctools_access_check',
  'all contexts' => TRUE,
  'settings form' => 'identity_has_key_ctools_access_settings',
  'summary' => 'identity_has_key_ctools_access_summary',
);

/**
 * Settings form for the 'identity has key' access plugin
 */
function identity_has_key_ctools_access_settings($form, &$form_state, $conf) {
  $form['settings']['name'] = array(
    '#title' => t('Key Name'),
    '#type' => 'textfield',
    '#description' => t('Enter the name of the key for which to test. Only keys with this name will pass the test. You may use panels context substitutions in this field.'),
    '#default_value' => $conf['name'],
  );

  // Provide a list of allowed context substitutions.
  // Freely copied from panels_edit_display_settings_form().  
  $header = array(t('Keyword'), t('Value'));
  $rows = array();
  $display = $form_state['display'];
  foreach ($display->context as $context) {
    foreach (ctools_context_get_converters('%' . check_plain($context->keyword) . ':', $context) as $keyword => $title) {
      $rows[] = array(
        check_plain($keyword),
        t('@identifier: @title', array('@title' => $title, '@identifier' => $context->identifier)),
      );
    }
  }
  $form['display_title']['contexts'] = array(
    '#type' => 'fieldset',
    '#title' => t('Substitutions'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#value' => theme('table', array('header' => $header, 'rows' => $rows)),
  );

  return $form;
}

/**
 * Check for access.
 */
function identity_has_key_ctools_access_check($conf, $context) {
  $id = identity_get_current();
  $group = ctools_context_keyword_substitute($conf['name'], array(), $context);
  return identity_key_get_keys($id, $group);
}

/**
 * Provide a summary description.
 */
function identity_has_key_ctools_access_summary($conf, $contexts) {
  return check_plain(t('Check for identity key named @name.', array('@name' => $conf['name'])));
}

