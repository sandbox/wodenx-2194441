<?php
/**
 * @file
 * Plugin to provide access control based on current identity.
 */

/**
 * Defines the plugin.
 */
$plugin = array(
  'title' => t("Identity exists?"),
  'description' => t('Control access based on Identity.'),
  'callback' => 'identity_ctools_access_current_identity',
  'default' => array(
    'description' => '',
    'browser' => '',
  ),
  'summary' => 'identity_ctools_access_current_identity_summary',
  'all contexts' => TRUE,
);

/**
 * Check for access.
 */
function identity_ctools_access_current_identity($conf, $contexts) {
  $id = identity_get_current();
  return !empty($id);
}

/**
 * Provide a summary description.
 */
function identity_ctools_access_current_identity_summary($conf, $contexts) {
  return check_plain(t('Check if a current identity exists.'));
}
