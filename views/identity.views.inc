<?php

/**
 * Implements hook views_plugins().
 */
function identity_views_plugins() {
  return array(
    'access' => array(
      'identity_access' => array(
        'title' => t('Identity'),
        'handler' => 'identity_plugin_access',
        'uses options' => TRUE,
        'path' => drupal_get_path('module', 'identity') . '/views',
      ),
    ),
  );
}
