<?php

/**
 * @file
 * Definition of views_plugin_access_perm.
 */

/**
 * Access plugin that provides permission-based access control.
 *
 * @ingroup views_access_plugins
 */
class identity_plugin_access extends views_plugin_access {
  function access($account) {
    return identity_views_access($this->options['condition']);
  }

  function get_access_callback() {
    return array('identity_views_access', array($this->options['condition']));
  }

  function summary_title() {
    switch ($this->options['condition']) {
      case 'exists':
        return t('Exists');
        break;
      case 'not_exists':
        return t('Does not exist');
        break;
      default:
        return t('Settings');
    }
    return $this->options['condition']['title'];
  }


  function option_definition() {
    $options = parent::option_definition();
    $options['condition'] = array('default' => 'exists');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['condition'] = array(
      '#type' => 'radios',
      '#title' => t('Condition'),
      '#options' => array(
        'exists' => t('Identity exists'),
        'not_exists' => t('Identity does not exist'),
      ),
      '#default_value' => $this->options['condition'],
      '#description' => t('Choose the identity-based condition which will control access.'),
    );
  }
}
